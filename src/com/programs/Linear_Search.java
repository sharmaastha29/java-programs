package com.programs;
import java.util.*;
public class Linear_Search {
public static void main(String[] args) {
	
	Scanner sc=new Scanner(System.in);
	System.out.println("enter the size of array: ");
	int n=sc.nextInt();
	int a[]=new int[n];
	System.out.println("enter the numbers: ");
	for(int i=0;i<n;i++){
		a[i]=sc.nextInt();
	}
	System.out.println("enter the number to search: ");
	int search=sc.nextInt();
	
	
	for(int i=0;i<n;i++){
		if(a[i]==search){
			System.out.println("Number found at location: "+i+1);
		}
	}
}
}

package com.programs;
import java.util.Scanner;
public class BinarySearch {
public static void main(String[] args) {
	
	Scanner sc=new Scanner(System.in);
	System.out.println("enter the size of array: ");
	int n=sc.nextInt();
	int a[]=new int[n];
	System.out.println("enter the numbers: ");
	for(int i=0;i<n;i++){
		a[i]=sc.nextInt();
	}
	
	System.out.println("enter the number to search: ");
	int search=sc.nextInt();
	
	int first=0, last=n-1, mid=(first+last)/2;
	
	while(first<=last){
		if(a[mid]==search){
			System.out.println("Number found at location: "+ (mid+1));
			break;
		}
		
		else if(a[mid]<search){
			first=mid+1;
					mid=(first+last)/2;
		}
		else{
			last=mid-1;
			mid=(first+last)/2;
		}
	}
	if(first>last){
		System.out.println(" No such number found ");
	}
}
}

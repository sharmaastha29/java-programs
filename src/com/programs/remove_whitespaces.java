package com.programs;
import java.util.Scanner;

public class remove_whitespaces {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("enter the string");
	String str=sc.nextLine();
	System.out.println("String before removing whitespaces: "+str);
	String st=str.replaceAll("\\s", "");
	System.out.println("String after removing whitespaces: "+st);
}
}

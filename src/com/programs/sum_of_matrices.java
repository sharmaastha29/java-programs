package com.programs;
import java.util.*;
public class sum_of_matrices {
public static void main(String[] args) {
	
	
	Scanner sc=new Scanner(System.in);
	System.out.println("enter no. of rows and columns: ");
	int n=sc.nextInt();
	int m=sc.nextInt();
	
	int a[][]=new int[n][m];
	int b[][]=new int[n][m];
	int sum[][]=new int[n][m];
	
	System.out.println("enter no's in first matrix: ");
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			a[i][j]=sc.nextInt();
		}
	}
	
	System.out.println("enter no's in second matrix: ");
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			b[i][j]=sc.nextInt();
		}
	}
	
	System.out.println("Sum of matrices is: ");
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			sum[i][j]=a[i][j]+b[i][j];
			System.out.print(sum[i][j]+" ");
			
		}
		System.out.println();
	}
}
}

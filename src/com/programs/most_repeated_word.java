package com.programs;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

public class most_repeated_word {
	public static void main(String[] args) {
		try {
			File f = new File("F://i.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				String s=line.replaceAll("[^\\sa-zA-Z]", "");
				System.out.println(s);
				String c[] = s.split(" ");
				HashMap<String, Integer> hm = new HashMap<String, Integer>();
				for (String ch : c) {
					if (hm.containsKey(ch)) {
						int j = hm.get(ch) + 1;
						hm.put(ch, j);
					} else {
						hm.put(ch, 1);
					}
				}

				System.out.println("hm is : " + hm);

				/*
				 * Set<String>k=hm.keySet(); for(String key:k){
				 * System.out.println(key);
				 * 
				 * }
				 */

				/*
				 * for(int i=0;i<hm.size();i++){
				 * if(hm.get(ch[i])>hm.get(ch[i+1])){
				 * 
				 * } }
				 */
				/*
				 * System.out.println(hm); List keys = new
				 * ArrayList(hm.values()); Collections.sort(keys);
				 * System.out.println(keys); Integer i = (Integer)
				 * Collections.max(keys); System.out.println(i);
				 */

				
				int frequency = 1;
				Set<Entry<String, Integer>> entrySet = hm.entrySet();

				for (Entry<String, Integer> entry : entrySet) {
					if (entry.getValue() > frequency) {
						

						frequency = entry.getValue();
						System.out.println("Frequency: "+frequency);
					}
				}
			
				HashMap<String, Integer> hm1 = new HashMap<String, Integer>();

				for (Entry<String, Integer> entry : entrySet) {
					if (entry.getValue() == frequency) {
						hm1.put(entry.getKey(), entry.getValue());

					}
				}

				System.out.println(hm1);
				/*
				 * if(frequency > 1) {System.out.println(
				 * "The most frequent element : "+element);
				 * 
				 * System.out.println("Its frequency : "+frequency);
				 * 
				 * }else{ frequency=1; }
				 * 
				 */
			}
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

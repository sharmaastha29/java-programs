package com.programs;

import java.io.*;
public class write_append_string_to_file {

	public static void main(String[] args) {
		
		try{
		String str="hi I am Astha. I love travelling.";
		File f=new File("F://abc.txt");
		if(!f.exists()){
			f.createNewFile();
		}
		FileWriter fw=new FileWriter(f,true);
		BufferedWriter bw=new BufferedWriter(fw);
		bw.write(str);
		bw.close();
		
	}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
